<?php 
class Person{
	public $name;
	public $age;
	public $id;
	public function __construct($name, $age)
	{
		# code...
		$this->name = $name;
		$this->age = $age;
	}
	/*public function personDetails(){
		echo "Person name is {$this->name} and person age is {$this->age}";
	}*/
	public function setID($id){
		$this->id = $id;
	}
	public function __destruct(){
		if(! empty($this->id)){
			echo "Saving person";
		}
	}
}
$personOne = new Person('aklima akther', '24');
//$personOne->personDetails();
$personOne->setID(10);
unset($personOne);
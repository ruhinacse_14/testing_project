<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Foreach loop</title>
</head>
<body>
	<?php 

	$person_info = array(
				     array(
				     	'ID' => '1', 'Name' => 'aklima', 'Age' => '23','Gender' => 'female'
				     	), 
					 array(
					 	'ID' => '2', 'Name' => 'akaa', 'Age' => '24','Gender' => 'female'
					 	),
					 array('ID' => '3', 'Name' => 'akEEaa', 'Age' => '25','Gender' => 'male'
					 	)
				); 

					
	?>
	<table border="1">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Age</th>
			<th>Gender</th>
		</tr>
		
				<?php
				foreach ($person_info as $key => $value) { ?>
				<tr>
					<td><?php echo $value['ID'];?></td>
					<td><?php echo $value['Name']; ?></td>
					<td><?php echo $value['Age']; ?></td>
					<td><?php echo $value['Gender']; ?></td>
				</tr>
						
				<?php	}
				?>
			
	</table>
</body>
</html>
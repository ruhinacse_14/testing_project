<?php 
if(isset($_POST['calculation'])){
	$f_name = $_POST['f_name'];
	$g_number = $_POST['g_number'];
	if(empty($f_name) or empty($g_number)){
		echo "<span style='color:red'>Field must not leave empty</span><br>";
		echo "<a href='/testing/index.php'>Please return back</a>";
	}
	else{
		if($g_number >= 90 && $g_number <= 100){
			echo "Congratulation!" .$f_name. "<br>";
			echo "You have got grade: A";
		}
		elseif($g_number >= 80 && $g_number <= 89){
			echo "Congratulation!" .$f_name. "<br>";
			echo "You have got grade: B";
		}
		elseif($g_number >= 70 && $g_number <= 79){
			echo "Congratulation!" .$f_name. "<br>";
			echo "You have got grade: B";
		}
		elseif($g_number >= 60 && $g_number <= 69){
			echo "Congratulation!" .$f_name. "<br>";
			echo "You have got grade: C";
		}
		elseif($g_number >= 50 && $g_number <= 59){
			echo "Congratulation!" .$f_name. "<br>";
			echo "You have got grade: B";
		}
		else{
			echo "Sorry You have been failed ".$f_name;
		}
	}
}
?>
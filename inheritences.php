<?php 

class userData{
	public $user;
	public $userId;
	protected $age = "25";

	public function __construct($userName, $userId){
		$this->user = $userName;
		$this->userId = $userId;
		
	}
	public function display(){
		echo "User name is {$this->user} and User id is {$this->userId}";
		echo "<br>";
		echo $this->age." inside class";

	}
}
class Admin extends UserData{
	public $level;
	public function display(){
		echo "User name is {$this->user} and User id is {$this->userId} and user level is {$this->level}";
		echo "<br>";
		echo $this->age." in the subclass class";
	}

}
$user = "aklima";
$id = "24";
$ur = new UserData($user, $id);
echo "<br>";
$ur->display();
echo "<br>";
$user = "admin";
$id = "1";
$ad = new Admin($user, $id);
$ad->level = "Administrator";
$ad->display();
var_dump($ad);